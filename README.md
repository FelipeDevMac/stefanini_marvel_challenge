# Stefanini_Challenge

This is the App created for the MOBILE DEV CODE CHALLENGE By Stefanini for iOS Development Position.

## Description

Marvel Heroes App, you can navigate thrue all marvel heroes,  search by their names and see their profile page, you can also select and save your favorite super heroes and check then in your super heros list.

* **HTTP Requests was made by code in swfit without external libs as requested in the challenge.**
* **Connection Status was made by code in swfit without external libs as requested in the challenge.**

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. 

## Prerequisites

It is  important to have the latest version of Xcode. 

## Installing 

```
Clone this repository and open the solution using Xcode, open terminal and run the command line (pod install) inside the app folder.
```
## Some screens

![alternativetext](https://bitbucket.org/FelipeDevMac/stefanini_marvel_challenge/raw/1f2902ae4969c27326942439563245da89dffdf9/Screenshots/Marvel_Home.png) 
![alternativetext](https://bitbucket.org/FelipeDevMac/stefanini_marvel_challenge/raw/1f2902ae4969c27326942439563245da89dffdf9/Screenshots/Marvel_Profile.png)


## Third-party components

| Plug-ins|
| ------------------- |
|Kingfisher|
|CryptoSwift|
|ColumnFlowLayout|
|SQLite|

### Kingfisher

Kingfisher is a powerful, pure-Swift library for downloading and caching images from the web.
[For more information access here](https://github.com/onevcat/Kingfisher)

### CryptoSwift

CryptoSwift is a growing collection of standard and secure cryptographic algorithms implemented in Swift, it was used to crypt information to md5 in order to access the marvel api.
[For more information access here](https://github.com/krzyzanowskim/CryptoSwift)

### ColumnFlowLayout

ColumnFlowLayout is  a custom organizer for your collections cell.

### SQLite

SQLite is a simple and lightweight SQLite wrapper for Swift. It allows all basic SQLite functionality including being able to bind values to parameters in an SQL statement, it was used to save the favorite characters from Marvel.

## Author

* **Pedro Felipe Machado**

## License

This project is licensed under the MIT License 


