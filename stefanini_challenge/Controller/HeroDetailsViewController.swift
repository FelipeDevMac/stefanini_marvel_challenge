//
//  HeroDetailsViewController.swift
//  stefanini_challenge
//
//  Created by Felipe Mac on 31/05/19.
//  Copyright © 2019 Felipe Mac. All rights reserved.
//

import UIKit
import Kingfisher

class HeroDetailsViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    @IBOutlet weak var heroImageView: UIImageView!
    @IBOutlet weak var heroDescription: UITextView!
    @IBOutlet weak var seriesCollectionView: UICollectionView!
    @IBOutlet weak var comicsCollectionView: UICollectionView!
    @IBOutlet weak var SeriesLabel: UILabel!
    @IBOutlet weak var ComicsLabel: UILabel!
    
    var comicsArray = [[String:AnyObject]]()
    var seriesArray = [[String:AnyObject]]()
    var heroName = ""
    var thumbPath = ""
    var heroDescriptionText = ""
    var id : Int?
    
    override func viewWillAppear(_ animated: Bool) {
        DispatchQueue.main.async {
            self.CheckInternetStatus()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setDelegates()
    }
    
    //MARK: - Functions
    
    func setDelegates() {
        comicsCollectionView.delegate = self
        comicsCollectionView.dataSource = self
        seriesCollectionView.delegate = self
        seriesCollectionView.dataSource = self
    }
    
    func displayHeroData() {
        DispatchQueue.main.async {
            if self.heroDescriptionText == ""{
                self.heroDescription.text = "No description available"
            }else{
                self.heroDescription.text = self.heroDescriptionText
            }
            self.heroImageView.layer.cornerRadius = 5
            self.heroImageView.layer.borderWidth = 1
            self.heroImageView.layer.borderColor = UIColor.black.cgColor
            self.SeriesLabel.font = UIFont(name: "Pacifico-Regular", size: 20)
            self.ComicsLabel.font = UIFont(name: "Pacifico-Regular", size: 20)
            Utils.formatNavigationTitleFontWithDefaultStyle(view: self, description: self.heroName)
        }
        
        let modifier = AnyModifier { request in
            var r = request
            r.timeoutInterval = 10
            return r
        }
        
        if let url = URL(string: thumbPath) {
            
            DispatchQueue.main.async {
                self.heroImageView.kf.setImage(with: url, placeholder: nil, options: [.requestModifier(modifier)], progressBlock: nil) { (image, error, cache, url) in
                    if error != nil {
                      self.heroImageView.image = #imageLiteral(resourceName: "MarvelNotFound")
                    }
                }
            }
            self.heroImageView.kf.indicator?.stopAnimatingView()
        }
    }
    
    func CheckInternetStatus() {
        
        Utils.monitor.pathUpdateHandler = { pathUpdateHandler in
            if pathUpdateHandler.status == .satisfied {
                print("Internet connection is on.")
                DispatchQueue.main.async {
                    self.displayHeroData()
                    self.getHero()
                }
            } else {
                print("There's no internet connection.")
                Utils.createSimpleAlert(view: self, title: "Conexão sem Internet", message: "Only the favorites module is available while you are not connected to the internet." )
            }
        }
        Utils.monitor.start(queue: Utils.queue)
    }
    
    func validateResultZero() {
        if comicsArray.count == 0 {
            ComicsLabel.isHidden = true
            comicsCollectionView.isHidden = true
        }else{
            ComicsLabel.isHidden = false
            comicsCollectionView.isHidden = false
            comicsCollectionView.reloadData()
        }
        if seriesArray.count == 0 {
            SeriesLabel.isHidden = true
            seriesCollectionView.isHidden = true
        }else{
            SeriesLabel.isHidden = false
            seriesCollectionView.isHidden = false
            seriesCollectionView.reloadData()
        }
    }
    
    func finishApiMethod() {
        Utils.removeLoadingScreen(view: self.view)
        self.validateResultZero()
    }
    
    //MARK: - CollectionView Delegates
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == comicsCollectionView {
            return comicsArray.count
        }
        if collectionView == seriesCollectionView {
            return seriesArray.count
        }
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == comicsCollectionView {
            
            if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ComicsCell", for: indexPath) as? ComicsCollectionViewCell {
                let object = Items(dictionary: comicsArray[indexPath.row])
                cell.object = object
                return cell
            }
            return UICollectionViewCell()
        }
        
        if collectionView == seriesCollectionView {
        
            if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SeriesCell", for: indexPath) as? SeriesCollectionViewCell {
                let object = Items(dictionary: seriesArray[indexPath.row])
                cell.object = object
                return cell
            }
        }
        return UICollectionViewCell()
    }
    
    //MARK: - Api's
    
    func getHero() {
    
        Utils.setLoadingScreen(view: self.view)
        
        let heroUrl = "\(ApiServiceURL.pathCharacterPlusId)\(id ?? 0)?"
        let url = URL(string: ApiServiceURL.path + heroUrl + ApiServiceURL.getCredencial())!
        
        let task = URLSession.shared.dataTask(with: url) {(data, response, error) in
            
            if error != nil || data == nil {
                print("Client error!")
                Utils.createSimpleAlert(view: self, title: "Error", message: "Someting went wrong, try again later.")
                return
            }
            
            guard let respons = response as? HTTPURLResponse, (200...299).contains(respons.statusCode) else {
                print("Server error!")
                if let response = response as? HTTPURLResponse {
                    let statusCode = response.statusCode
                    Utils.createSimpleAlert(view: self, title: "Error", message: "\(ErrorTreatment.jsonErrorTreatment(error: statusCode))")
                }
                return
            }
            do {
                let jsonDict = try JSONSerialization.jsonObject(with: data!) as! Dictionary<String, AnyObject>
                if let data = jsonDict["data"] as? [String:AnyObject] {
                    if let result = data["results"] as? [[String:AnyObject]] {
                        if let comics = result[0]["comics"] as? [String:AnyObject] {
                            if let items = comics["items"] as? [[String:AnyObject]] {
                                self.comicsArray = items
                            }
                        }
                        if let series = result[0]["series"] as? [String:AnyObject] {
                            if let items = series["items"] as? [[String:AnyObject]] {
                                self.seriesArray = items
                            }
                        }
                        DispatchQueue.main.async {
                            self.finishApiMethod()
                        }  
                    }
                }
            } catch {
                Utils.createSimpleAlert(view: self, title: "Error", message: "Someting went wrong, try again later.")
            }
        }
        task.resume()
    }
}

