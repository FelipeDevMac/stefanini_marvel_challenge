//
//  FavoritesViewController.swift
//  stefanini_challenge
//
//  Created by Felipe Mac on 31/05/19.
//  Copyright © 2019 Felipe Mac. All rights reserved.
//

import UIKit
import SQLite3

class FavoritesViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, GetIndexBtnFromCollectionCellFavoritesDelegate {
    
    @IBOutlet weak var favoritesCollectionView: UICollectionView!
    
    var arrayFromDb : [[String:AnyObject]] = []
    
    let columnLayout = ColumnFlowLayout(
        cellsPerRow: 3,
        minimumInteritemSpacing: 10,
        minimumLineSpacing: 10,
        sectionInset: UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
    )
    
    override func viewWillAppear(_ animated: Bool) {
        getFavoritesFromDB()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setDelegates()
        updateDisplay()
    }
    
    //MARK: - Functions
    
    func updateDisplay() {
        Utils.formatNavigationTitleFontWithDefaultStyle(view: self, description: "Favorites")
        favoritesCollectionView?.collectionViewLayout = columnLayout
        favoritesCollectionView?.contentInsetAdjustmentBehavior = .always
    }
    
    func setDelegates() {
        favoritesCollectionView.delegate = self
        favoritesCollectionView.dataSource = self
    }
    
    func GetIndexFromCellToController(cell: FavoritesCollectionViewCell) {
        if let indexPath = self.favoritesCollectionView.indexPath(for: cell) {
            guard let _ = self.favoritesCollectionView.cellForItem(at: indexPath) as? FavoritesCollectionViewCell else { return }
            
            if let idFromService = cell.object?.id {
                let comandoCheckIfIsFavoriteOrNot = "SELECT * from MyHeros WHERE Favorite = 'true' AND IDFromService = '\(idFromService)'"
                
                if(sqlite3_exec(Utils.dataBase, comandoCheckIfIsFavoriteOrNot, nil, nil, nil)) == SQLITE_OK  {
                    
                    var resultado : OpaquePointer? = nil
                    
                    if (sqlite3_prepare_v2(Utils.dataBase, comandoCheckIfIsFavoriteOrNot, -1, &resultado, nil) == SQLITE_OK) {
                        
                        if sqlite3_step(resultado) == SQLITE_ROW {
                               
                        }else{
                            self.removeHeroAsFavorite(cell: cell)
                        }
                        sqlite3_finalize(resultado)
                    }
                }else{
                    print("SQL Select Error")
                }
            }
        }
    }
    
    func validateResultZero() {
        if arrayFromDb.count == 0 {
            
            let alert = UIAlertController(title: "Marvel Heros", message: "There are no heroes added to your Favorites list.", preferredStyle: UIAlertController.Style.alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .default) { (action) in                
                Utils.removeLoadingScreen(view: self.view)
            }
            alert.addAction(OKAction)
            self.present(alert, animated: true, completion: nil)
        }else{
         
        }
    }
    
    func removeHeroAsFavorite(cell: FavoritesCollectionViewCell) {
        
        if let id = cell.object?.id {
            
            let comandoDelete = "DELETE from MyHeros WHERE ID_Hero = '\(id)'"
            
            if(sqlite3_exec(Utils.dataBase, comandoDelete, nil, nil, nil)) == SQLITE_OK  {
                
                let alert = UIAlertController(title: "Marvel Heroes", message: "Hero successfully removed from Favorites!", preferredStyle: UIAlertController.Style.alert)
                
                let OKAction = UIAlertAction(title: "Ok", style: .default) { (action) in
                    print("Deleted Successfully")
                    self.getFavoritesFromDB()
                }
                alert.addAction(OKAction)
                
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    //MARK: - CollectionView Delegates
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrayFromDb.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if let cell = favoritesCollectionView.dequeueReusableCell(withReuseIdentifier: "FavoritesCollection", for: indexPath) as? FavoritesCollectionViewCell {
            
            let charactes = DataBaseObject(dictionary: arrayFromDb[indexPath.row])
            cell.object = charactes
            cell.delegate = self
            
            return cell
        }
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let cell = favoritesCollectionView.dequeueReusableCell(withReuseIdentifier: "FavoritesCollection", for: indexPath) as? FavoritesCollectionViewCell {
            
            let hero = DataBaseObject(dictionary: arrayFromDb[indexPath.row])
            cell.object = hero
            
            if let controller = storyboard?.instantiateViewController(withIdentifier: "HeroDetails") as? HeroDetailsViewController {
                
                if let id = cell.object?.idFromService {
                    
                    if let heroName = cell.object?.name {
                        controller.heroName = heroName
                    }
                    if let heroDescription = cell.object?.description {
                        controller.heroDescriptionText = heroDescription
                    }                
                    if let thumbPath = cell.object?.extensionThumb {
                        controller.thumbPath = thumbPath
                    }
                    
                    controller.id = id
                    navigationController?.pushViewController(controller, animated: true)
                }
            }
        }
    }
    
    func getFavoritesFromDB() {
        arrayFromDb = []
        
        if (FileManager.default.fileExists(atPath: Utils.pathArquivo)) {
            
            if(sqlite3_open(Utils.pathArquivo, &Utils.dataBase) == SQLITE_OK)  {
                
                let selectAllHeroes = "SELECT * from MyHeros"
                
                if (sqlite3_prepare_v2(Utils.dataBase, selectAllHeroes, -1, &Utils.resultado, nil) == SQLITE_OK) {
                    
                    if (sqlite3_step(Utils.resultado) == SQLITE_ROW) {
                        
                        while sqlite3_step(Utils.resultado) == SQLITE_ROW {
                            
                            let ID_Hero = sqlite3_column_int(Utils.resultado, 0)
                            
                            let IDFromService = sqlite3_column_int(Utils.resultado, 1)
                            
                            let Hero_Name = String(cString: sqlite3_column_text(Utils.resultado, 2))
                            
                            let Hero_Url  = String(cString: sqlite3_column_text(Utils.resultado, 3))
                            
                            let Hero_Description  = String(cString: sqlite3_column_text(Utils.resultado, 5))
                            
                            var dicionario : [String:AnyObject] = [:]
                            
                            dicionario["id"] = ID_Hero as AnyObject?
                            dicionario["idFromService"] = IDFromService as AnyObject?
                            dicionario["name"] = Hero_Name as AnyObject?
                            dicionario["extensionThumb"] = Hero_Url as AnyObject?
                            dicionario["description"] = Hero_Description as AnyObject?
                            
                            self.arrayFromDb.append(dicionario)
                        }
                    }
                    sqlite3_finalize(Utils.resultado)
                    
                }else{
                    let errorMessage = String(cString: sqlite3_errmsg(Utils.dataBase)!)
                    print("Query could not be prepared! \(errorMessage)")
                }
                
                favoritesCollectionView.reloadData()
                self.validateResultZero()
            }
            
        }else{
            print ("Database opened error")
        }
    }
}
