//
//  HomeViewController.swift
//  stefanini_challenge
//
//  Created by Felipe Mac on 31/05/19.
//  Copyright © 2019 Felipe Mac. All rights reserved.
//

import UIKit
import SQLite3

class HomeViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UISearchBarDelegate, GetIndexBtnFromCollectionCellHomeDelegate {
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var searchButton: UIBarButtonItem!
    @IBOutlet weak var searchBarHeight: NSLayoutConstraint!
    
    var charactersArray : [DataObject] = []
    var filteredcharactersArray : [DataObject] = []
    var basecharactersArray : [DataObject] = []
    var offset = 0
    
    let columnLayout = ColumnFlowLayout(
        cellsPerRow: 3,
        minimumInteritemSpacing: 10,
        minimumLineSpacing: 10,
        sectionInset: UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
    )
    
    @IBOutlet weak var HomeCollectionView: UICollectionView!
    
    override func viewWillAppear(_ animated: Bool) {
        CheckDBIfExistIfNotCreateDB()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        CheckInternetStatus()
        setDelegates()
        setConfigurationsToDisplayInUI()
    }
    
    //MARK: - IBActions
    
    @IBAction func checkSearchBoxState(_ sender: UIBarButtonItem) {
        if searchBarHeight.constant == 0 {
            activateSearchBox()
        }else{
            desactivateSearchBox()
        }
    }
    
    //MARK: - Functions
    
    func activateSearchBox() {
        searchBar.text = ""
        
        UIView.animate(withDuration: 0.1,
                       delay: 0.1,
                       options: UIView.AnimationOptions.curveEaseIn,
                       animations: { () -> Void in
                        self.searchBarHeight.constant = 56
                        self.view.layoutIfNeeded()
                        self.resetArticleArray()
        }, completion: { (finished) -> Void in
        })
    }
    
    func desactivateSearchBox() {
        
        UIView.animate(withDuration: 0.1,
                       delay: 0.1,
                       options: UIView.AnimationOptions.curveEaseIn,
                       animations: { () -> Void in
                        self.searchBarHeight.constant = 0
                        self.view.layoutIfNeeded()
                        self.view.endEditing(true)
                        self.resetArticleArray()
        }, completion: { (finished) -> Void in
        })
    }
    
    func setDelegates() {
        self.searchBar.delegate = self
        HomeCollectionView.delegate = self
        HomeCollectionView.dataSource = self
    }
    
    func setConfigurationsToDisplayInUI() {
        self.searchBar.searchBarStyle = .minimal
        self.searchBar.returnKeyType = UIReturnKeyType.done
        self.searchBar.placeholder = "Hero name"
        searchBarHeight.constant = 0
        HomeCollectionView?.collectionViewLayout = columnLayout
        HomeCollectionView?.contentInsetAdjustmentBehavior = .always
        HomeCollectionView.addSubview(refreshControl)
        Utils.formatNavigationTitleFontWithDefaultStyle(view: self, description: "Characters")
    }
    
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(HomeViewController.handleRefresh(_:)), for: UIControl.Event.valueChanged)
        
        return refreshControl
    }()
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        getCharacters(offset: 0, presentLoadingView: true)
        self.HomeCollectionView.reloadData()
        refreshControl.endRefreshing()
    }
    
    func preparedb() {
        let comm = "INSERT into MyHeros values (NULL, '999', 'Null', 'NUll', '\(true)', 'Null')"
        if(sqlite3_exec(Utils.dataBase, comm, nil, nil, nil)) == SQLITE_OK  {}
    }
    
    func addHeroAsFavorite(cell: HomeCollectionViewCell) {
        
        if var name = cell.object?.name {
            
            if name.contains("'") {
                name = name.replacingOccurrences(of: "'", with: "")
            }
            
            if let path = cell.object?.thumbnail?.path {
                
                if let extensionThumb = cell.object?.thumbnail?.extensionThumb {
                    
                    let url = "\(path).\(extensionThumb)"
                    
                    if var description = cell.object?.description {
                        
                        if description.contains("'") {
                            description = description.replacingOccurrences(of: "'", with: "")
                        }
                        
                        if let idFromService = cell.object?.id {
                            
                            let comandoCadastro = "INSERT into MyHeros values (NULL, '\(idFromService)', '\(name)', '\(url)', '\(true)', '\(description)')"
                            
                            if(sqlite3_exec(Utils.dataBase, comandoCadastro, nil, nil, nil)) == SQLITE_OK  {
                                
                                let alert = UIAlertController(title: "Marvel Heroes", message: "Hero successfully added to your Favorites!", preferredStyle: UIAlertController.Style.alert)
                                
                                let OKAction = UIAlertAction(title: "Ok", style: .default) { (action) in
                                    print("Saved Successfully")
                                    self.HomeCollectionView.reloadData()
                                }
                                alert.addAction(OKAction)
                                
                                self.present(alert, animated: true, completion: nil)
                                
                            }else{
                                print("SQL Insert Error")
                                print(sqlite3_errmsg)
                            }
                        }
                    }
                }
            }
        }
    }
    
    func removeHeroAsFavorite(cell: HomeCollectionViewCell) {
        
        if let idFromService = cell.object?.id {
            
            let comandoDelete = "DELETE from MyHeros WHERE IDFromService = '\(idFromService)'"
            
            if(sqlite3_exec(Utils.dataBase, comandoDelete, nil, nil, nil)) == SQLITE_OK  {
                
                let alert = UIAlertController(title: "Marvel Heroes", message: "Hero successfully removed from Favorites!", preferredStyle: UIAlertController.Style.alert)
                
                let OKAction = UIAlertAction(title: "Ok", style: .default) { (action) in
                    print("Deleted Successfully")
                    DispatchQueue.main.async {
                        self.HomeCollectionView.reloadData()
                    }
                }
                alert.addAction(OKAction)
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    func GetIndexFromCellToController(cell: HomeCollectionViewCell) {
        if let indexPath = self.HomeCollectionView.indexPath(for: cell) {
            guard let cell = self.HomeCollectionView.cellForItem(at: indexPath) as? HomeCollectionViewCell else { return }
            
            if let idFromService = cell.object?.id {
                let comandoCheckIfIsFavoriteOrNot = "SELECT * from MyHeros WHERE Favorite = 'true' AND IDFromService = '\(idFromService)'"
                
                if(sqlite3_exec(Utils.dataBase, comandoCheckIfIsFavoriteOrNot, nil, nil, nil)) == SQLITE_OK  {
                    
                    var resultado : OpaquePointer? = nil
                    
                    if (sqlite3_prepare_v2(Utils.dataBase, comandoCheckIfIsFavoriteOrNot, -1, &resultado, nil) == SQLITE_OK) {
                        
                        if sqlite3_step(resultado) == SQLITE_ROW {
                            
                            self.removeHeroAsFavorite(cell: cell)
                        }else{
                            self.addHeroAsFavorite(cell: cell)
                        }
                        sqlite3_finalize(resultado)
                    }
                }else{
                    print("SQL Select Error")
                }
            }
        }
    }
    
    func CheckDBIfExistIfNotCreateDB(){
        print("Path:\(Utils.pathArquivo)")
        if (FileManager.default.fileExists(atPath: Utils.pathArquivo)) {
            
            if(sqlite3_open(Utils.pathArquivo, &Utils.dataBase) == SQLITE_OK)  {
                print("Database opened successfully")
            }else{
                print ("Database opened error")
            }
        }else{
            if (sqlite3_open(Utils.pathArquivo, &Utils.dataBase) == SQLITE_OK) {
                print("Database created Successfully")
                
                let comando1 = " create table if not exists MyHeros (ID_Hero INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, IDFromService INTERGER, Hero_Name TEXT, Hero_Url TEXT, Favorite Bool, Hero_Description TEXT )"
                
                if (sqlite3_exec(Utils.dataBase, comando1, nil, nil, nil)) == SQLITE_OK {
                    print("Table created Successfully")
                    preparedb()
                }else{
                    print("Table created Error \(String(describing: sqlite3_errmsg(Utils.dataBase)))")
                }
            }else{
                print("Database created Error")
            }
        }
    }
    
    func CheckInternetStatus() {
        
        Utils.monitor.pathUpdateHandler = { pathUpdateHandler in
            if pathUpdateHandler.status == .satisfied {
                print("Internet connection is on.")
                DispatchQueue.main.async {
                    self.getCharacters(offset: 0, presentLoadingView: true)
                }
            } else {
                print("There's no internet connection.")
                Utils.createSimpleAlert(view: self, title: "No internet connection", message: "Only the favorites module is available while you are not connected to the internet." )
            }
        }
        Utils.monitor.start(queue: Utils.queue)
    }
    
    func validateResultZero() {
        if charactersArray.count == 0 {
            let alert = UIAlertController(title: "Marvel Heros", message: "We have no heroes at this time, please try again later.", preferredStyle: UIAlertController.Style.alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .default) { (action) in
                Utils.removeLoadingScreen(view: self.view)
            }
            alert.addAction(OKAction)
            self.present(alert, animated: true, completion: nil)
        }else{
            HomeCollectionView.reloadData()
        }
    }
    
    func resetArticleArray() {
        self.charactersArray = self.basecharactersArray
        self.HomeCollectionView.reloadData()
    }
    
    //MARK: - SearchBar Delegates
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        view.endEditing(true)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        filterContentForSearchText(searchText: searchBar.text!)
        HomeCollectionView.reloadData()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        resetArticleArray()
    }
    
    func filterContentForSearchText(searchText: String, scope: String = "All") {
        if searchText != "" {
            charactersArray = filteredcharactersArray.filter {data in
                return data.name?.lowercased().contains(searchText.lowercased()) ?? false
            }
        }else { self.charactersArray = self.basecharactersArray}
    }
    
    func finishApiMethod() {
        self.filteredcharactersArray = self.charactersArray
        self.basecharactersArray = self.charactersArray
        
        Utils.removeLoadingScreen(view: self.view)
        self.validateResultZero()
    }
    
    //MARK: - ScrollView Delegates
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let actualPosition = scrollView.contentOffset.y
        let contentHeight = scrollView.contentSize.height - 1000
        
        if (actualPosition >= contentHeight) {
            offset += 20
            DispatchQueue.main.async {
                self.getCharacters(offset: self.offset, presentLoadingView: false)
            }
        }
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
       return charactersArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if let cell = HomeCollectionView.dequeueReusableCell(withReuseIdentifier: "HomeCollection", for: indexPath) as? HomeCollectionViewCell {
            let charactes = charactersArray[indexPath.row]
            cell.object = charactes
            cell.delegate = self
            return cell
            
        }
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeCollection", for: indexPath) as? HomeCollectionViewCell {
            
            let hero = charactersArray[indexPath.row]
            cell.object = hero
            
            if let controller = storyboard?.instantiateViewController(withIdentifier: "HeroDetails") as? HeroDetailsViewController {
                
                if let id = cell.object?.id {
                    controller.id = id
                    if let heroName = cell.object?.name {
                        controller.heroName = heroName
                    }
                    if let thumbPath = cell.object?.thumbnail?.path {
                        
                        if let extensionThumb = cell.object?.thumbnail?.extensionThumb {
                            
                            let path = "\(thumbPath).\(extensionThumb)"
                            controller.thumbPath = path
                        }
                    }
                    if let heroDescription = cell.object?.description {
                        controller.heroDescriptionText = heroDescription
                    }
                    navigationController?.pushViewController(controller, animated: true)
                }
            }
        }
    }
    
    //MARK: - Api's
    
    func getCharacters(offset: Int = 0, presentLoadingView: Bool = true) {
        
        if presentLoadingView {
            Utils.setLoadingScreen(view: self.view)
            self.charactersArray = []
        }
        
        let queryParams: [String:String] = ["offset": String(offset), "limit": String(ApiServiceURL.limit)]
        let url = URL(string: ApiServiceURL.path + ApiServiceURL.pathCharacters + queryParams.queryString! + ApiServiceURL.getCredencial())!
        
        let task = URLSession.shared.dataTask(with: url) {(data, response, error) in
            
            if error != nil || data == nil {
                print("Client error!")
                Utils.createSimpleAlert(view: self, title: "Error", message: "Someting went wrong, try again later.")
                return
            }
            
            guard let respons = response as? HTTPURLResponse, (200...299).contains(respons.statusCode) else {
                print("Server error!")
                if let response = response as? HTTPURLResponse {
                    let statusCode = response.statusCode
                    Utils.createSimpleAlert(view: self, title: "Error", message: "\(ErrorTreatment.jsonErrorTreatment(error: statusCode))")
                }
                return
            }
            do {
                let jsonDict = try JSONSerialization.jsonObject(with: data!) as! Dictionary<String, AnyObject>
                if let data = jsonDict["data"] as? [String:AnyObject] {
                    if let result = data["results"] as? [[String:AnyObject]] {
                        for item in result {
                            self.charactersArray.append(DataObject(dictionary: item))
                        }
                    }
                }
            } catch {
                Utils.createSimpleAlert(view: self, title: "Error", message: "Someting went wrong, try again later.")
            }
            DispatchQueue.main.async {
                 self.finishApiMethod()
            }
        }
        task.resume()
    }
}
