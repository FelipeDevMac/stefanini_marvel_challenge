//
//  TabPrincipalViewController.swift
//  stefanini_challenge
//
//  Created by Felipe Mac on 31/05/19.
//  Copyright © 2019 Felipe Mac. All rights reserved.
//

import UIKit

class TabPrincipalViewController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        configurDisplay()
    }
    
    func configurDisplay() {
        
        UINavigationBar.appearance().barTintColor = UIColor.black
        UINavigationBar.appearance().tintColor = UIColor.white
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor:UIColor.white]
        
        self.tabBar.backgroundImage = UIImage(named: "")
        self.tabBar.tintColor = UIColor.white
        self.tabBar.barTintColor = UIColor.black
        self.tabBar.isTranslucent = true
        self.tabBar.autoresizesSubviews = false
        self.tabBar.clipsToBounds = true
    }
}

