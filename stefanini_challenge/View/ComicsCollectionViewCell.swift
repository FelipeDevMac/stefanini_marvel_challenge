//
//  ComicsCollectionViewCell.swift
//  stefanini_challenge
//
//  Created by Felipe Mac on 31/05/19.
//  Copyright © 2019 Felipe Mac. All rights reserved.
//

import UIKit

class ComicsCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var comicsImage: UIImageView!
    @IBOutlet weak var comicsName: UILabel!
    
    var object : Items? {
        didSet {
            if let data = object {

                if let name = data.name {
                    comicsName.text = name
                }
            }
        }
    }
}


