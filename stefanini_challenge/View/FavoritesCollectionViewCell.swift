//
//  FavoritesCollectionViewCell.swift
//  stefanini_challenge
//
//  Created by Felipe Mac on 31/05/19.
//  Copyright © 2019 Felipe Mac. All rights reserved.
//

import UIKit
import SQLite3
import Kingfisher

protocol GetIndexBtnFromCollectionCellFavoritesDelegate {
    func GetIndexFromCellToController(cell: FavoritesCollectionViewCell)
}

class FavoritesCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var favoritesFavoriteBtn: UIButton!
    @IBOutlet weak var favoritesImage: UIImageView!
    @IBOutlet weak var favoritesDescription: UILabel!
    
    var delegate: GetIndexBtnFromCollectionCellFavoritesDelegate?
    
    @IBAction func GetIndexFromCellToController(_ sender: UIButton) {
        if let _ = delegate {
            delegate?.GetIndexFromCellToController(cell: self)
        }
    }
    var object : DataBaseObject? {
        didSet {
            if let data = object {
                
                favoritesImage.layer.cornerRadius = 10                
                favoritesDescription.text = data.name
                
                if let path = data.extensionThumb {
                    
                    self.favoritesImage.image = #imageLiteral(resourceName: "noInternetConnection")
                    favoritesFavoriteBtn.setImage(#imageLiteral(resourceName: "favoritesSelected"), for: .normal)
                        
                    let modifier = AnyModifier { request in
                        var r = request
                        r.timeoutInterval = 3
                        return r
                    }
                    
                    if let url = URL(string: path) {
                            
                        DispatchQueue.main.async {
                            self.favoritesImage.kf.setImage(with: url, placeholder: nil, options: [.requestModifier(modifier)], progressBlock: nil) { (image, error, cache, url) in
                                if error != nil {
                                    print(error?.localizedDescription ?? "Could not be able to download image")
                                }
                            }
                        }
                        self.favoritesImage.kf.indicator?.stopAnimatingView()
                    }
                }
            }
        }
    }
}

