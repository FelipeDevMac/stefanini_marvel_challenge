//
//  HomeCollectionViewCell.swift
//  stefanini_challenge
//
//  Created by Felipe Mac on 31/05/19.
//  Copyright © 2019 Felipe Mac. All rights reserved.
//

import UIKit
import SQLite3
import Kingfisher

protocol GetIndexBtnFromCollectionCellHomeDelegate {
    func GetIndexFromCellToController(cell: HomeCollectionViewCell)
}

class HomeCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var homeFavoriteBtn: UIButton!
    @IBOutlet weak var homeImage: UIImageView!
    @IBOutlet weak var HomeDescription: UILabel!
    
    var delegate: GetIndexBtnFromCollectionCellHomeDelegate?
    
    @IBAction func GetIndexFromCellToController(_ sender: UIButton) {        
        if let _ = delegate {
            delegate?.GetIndexFromCellToController(cell: self)
        }
    }
    
    func updateUI() {
        
        homeImage.layer.cornerRadius = 10
        
        if let name = object?.name {
            HomeDescription.text = name
        }
        
        if let thumbnailData = object?.thumbnail {
            
            if let path = thumbnailData.path {
                
                if let extensionThumb = thumbnailData.extensionThumb {
                    
                    let thumbPath = "\(path).\(extensionThumb)"
                    
                    homeImage.image = #imageLiteral(resourceName: "loading")
                    
                    let modifier = AnyModifier { request in
                        var r = request
                        r.timeoutInterval = 3
                        return r
                    }
                    
                    if let url = URL(string: thumbPath) {
                            
                        DispatchQueue.main.async {
                            self.homeImage.kf.setImage(with: url, placeholder: nil, options: [.requestModifier(modifier)], progressBlock: nil) { (image, error, cache, url) in
                                if error != nil {
                                    print(error?.localizedDescription ?? "Could not be able to download image")
                                }
                            }
                        }
                        self.homeImage.kf.indicator?.stopAnimatingView()
                    }
                }
            }
        }
    }
    
    func checkFavoritesFromDB() {
        if (FileManager.default.fileExists(atPath: Utils.pathArquivo)) {
            
            if(sqlite3_open(Utils.pathArquivo, &Utils.dataBase) == SQLITE_OK)  {
                
                if let idFromService = object?.id {
                    let comandoCheckIfIsFavoriteOrNot = "SELECT * from MyHeros WHERE Favorite = 'true' AND IDFromService = '\(idFromService)'"
                    
                    if(sqlite3_exec(Utils.dataBase, comandoCheckIfIsFavoriteOrNot, nil, nil, nil)) == SQLITE_OK  {
                        
                        var resultado : OpaquePointer? = nil
                        
                        if (sqlite3_prepare_v2(Utils.dataBase, comandoCheckIfIsFavoriteOrNot, -1, &resultado, nil) == SQLITE_OK) {
                            
                            if sqlite3_step(resultado) == SQLITE_ROW {
                                
                                homeFavoriteBtn.setImage(#imageLiteral(resourceName: "favoritesSelected"), for: .normal)
                            }else{
                                homeFavoriteBtn.setImage(#imageLiteral(resourceName: "favoritesUnselected"), for: .normal)
                            }
                            sqlite3_finalize(resultado)
                        }
                    }else{
                        print("SQL Select Error")
                    }
                }
            }else{
                print ("Database opened error")
            }
        }
    }
    
    var object : DataObject? {
        didSet {
            updateUI()
            checkFavoritesFromDB()
        }
    }
}
