//
//  SeriesCollectionViewCell.swift
//  stefanini_challenge
//
//  Created by Felipe Mac on 31/05/19.
//  Copyright © 2019 Felipe Mac. All rights reserved.
//

import UIKit

class SeriesCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var seriesImage: UIImageView!
    @IBOutlet weak var seriesName: UILabel!
    
    var object : Items? {
        didSet {
            if let data = object {
                
                if let name = data.name {
                    seriesName.text = name
                }
            }
        }
    }
}
