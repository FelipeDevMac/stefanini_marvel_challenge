//
//  Utils.swift
//  stefanini_challenge
//
//  Created by Felipe Mac on 31/05/19.
//  Copyright © 2019 Felipe Mac. All rights reserved.
//

import UIKit
import Network

class Utils: NSObject {

    static let imageCache = NSCache<AnyObject, AnyObject>()
    static let loadingView = UIView()
    static let spinner = UIActivityIndicatorView()
    
    static let pathArquivo = (NSHomeDirectory() as NSString).appendingPathComponent("Documents/arquivo.sqlite")
    static var dataBase : OpaquePointer?
    static var resultado : OpaquePointer?
    
    static let monitor = NWPathMonitor()
    static let queue = DispatchQueue(label: "InternetConnectionMonitor")
    
    static let columnLayout = ColumnFlowLayout(
        cellsPerRow: 3,
        minimumInteritemSpacing: 10,
        minimumLineSpacing: 10,
        sectionInset: UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
    )
    
    static func formatNavigationTitleFontWithDefaultStyle(view: UIViewController, description: String) {
        view.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        view.navigationController?.navigationBar.shadowImage = UIImage()
        view.navigationController?.navigationBar.isTranslucent = true
        view.navigationController?.view.backgroundColor = .clear
        view.navigationController?.navigationBar.tintColor = UIColor.white
        view.navigationItem.title = description
        let titleDict: NSDictionary = [NSAttributedString.Key.foregroundColor: UIColor.white, NSAttributedString.Key.font: UIFont(name: "Pacifico-Regular", size: 20)!]
        view.navigationController?.navigationBar.titleTextAttributes = titleDict as? [NSAttributedString.Key : Any]
    }
    
    //MARK: - Loadings
    
    static func setLoadingScreen(view: UIView) {
        loadingView.isHidden = false
        
        loadingView.isHidden = false
        loadingView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
        loadingView.backgroundColor = UIColor.gray.withAlphaComponent(0.3)
        
        // Sets spinner
        spinner.style = UIActivityIndicatorView.Style.gray
        spinner.frame = CGRect(x: 0, y: 0, width: view.frame.maxX, height: view.frame.maxY)
        spinner.startAnimating()
        
        view.addSubview(loadingView)
        loadingView.addSubview(self.spinner)
    }
    
    static func removeLoadingScreen(view: UIView) {
        self.spinner.stopAnimating()
        self.loadingView.isHidden = true
        self.loadingView.removeFromSuperview()
    }
    
    //MARK: - Alerts
    
    static func createSimpleAlert(view: UIViewController, title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        let OKAction = UIAlertAction(title: "OK", style: .default) { (action) in
        }
        alert.addAction(OKAction)
        view.present(alert, animated: true, completion: nil)
    }
}
