//
//  ErrorTreatment.swift
//  stefanini_challenge
//
//  Created by Pedro Machado on 11/02/19.
//  Copyright © 2019 Pedro Machado. All rights reserved.
//

import UIKit

class ErrorTreatment: NSObject {
    
    static func jsonErrorTreatment(error:Int) -> String {
        var message = String()
        switch error {
        case 400:
            message = "Bad request."
        case 404:
            message = "The resource you requested could not be found."
        default:
            message = "Something went wrong, please try again later."
        }
        return message
    }
}

